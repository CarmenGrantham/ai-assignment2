import samples
import util
import random
from game import Agent, Directions, Actions
from sklearn.tree import DecisionTreeClassifier
from sklearn.feature_extraction import DictVectorizer
from sklearn.preprocessing import LabelEncoder
from assignment2 import extract_action_features
from samples import extract_features

#
# feature extraction for Pacman
#
ALL_ACTIONS = sorted(Actions._directions.keys())

class ClassifierAgent(Agent):
    def __init__(self, training_set=None):
        super().__init__()
        training_data = samples.read_dataset(training_set)
        self.train(training_data)

    def train(self, training_data):
        """
        Train the agent using data set training_data.
        The data set comprises a dict with keys 'states', which maps to a list of game states, and 
        'actions', which maps to a list of actions corresponding to the list of game states.
        Store the resulting classifier in self.classifier
        """

        self.vectorizer = DictVectorizer()
        label_encoder = LabelEncoder()
        label_encoder.fit(ALL_ACTIONS)

        # Get the features to use on training data
        features_train = [extract_features(state, extract_action_features) for state in training_data['states']]
        X_train = self.vectorizer.fit_transform(features_train)
        y_train = label_encoder.transform(training_data['actions'])


        # Use the Decision Tree Classifier as it gave the best results during training and testing
        # - max_depth - Running optimisation code found that using max_depth of 21 improved results slightly
        self.classifier = DecisionTreeClassifier(max_depth=21)
        self.classifier.fit(X_train, y_train)




    def getAction(self, state):
        """
        getAction chooses the action predicted by the classifier that was learned from training_set.
        The classifier is accessible as self.classifier
        
        Just like in the practical project, getAction takes a GameState and returns
        some X in the set Directions.{North, South, West, East, Stop}.
        
        The returned action must be one of state.getLegalActions().
        """
        
        # Get the features from the GameState
        features = [extract_features(state, extract_action_features)]
        
        X = self.vectorizer.transform(features)

        # From training data get next move prediction
        y_pred = self.classifier.predict(X)
        action = ALL_ACTIONS[y_pred]

        # Get legal moves for this game state, the predicted move SHOULD be in this range
        # but need to double check first.
        legalMoves = state.getLegalPacmanActions()

        if (action in legalMoves):
            # Action is legal so return it
            print("{} is a legal action".format(action))
            return action
        else:
            # Use random move when predicted action is illegal - SHOULDN'T HAPPEN
            randomAction = random.choice(legalMoves)
            print("{} is NOT a legal action, using {} instead".format(action, randomAction))
            return randomAction
        