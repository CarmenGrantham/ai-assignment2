#
# COMP2019 ASSIGNMENT 2 2017
#
import heapq
import util
import numpy as np
from util import manhattanDistance
from samples import prepare_dataset
from decisionTreeUtils import extract_decisiontree_rules
from sklearn.metrics import classification_report, accuracy_score
from sklearn.tree import DecisionTreeClassifier
from sklearn.neighbors import KNeighborsClassifier
from sklearn.model_selection import GridSearchCV, cross_val_score, learning_curve, ShuffleSplit
from game import Actions
import matplotlib.pyplot as plt

#
# QUESTION 1
#

def extract_action_features(gameState, action):
    """
    This function extracts features reflecting the effects of applying action to gameState.

    You should return a dict object of features where keys are the feature names and 
    the values the feature values. The feature names must be the same for all actions.

    All values must be of primitive type (boolean, int, float) that scikit-learn can handle.
    """
    features = dict()
    successorState = gameState.generateSuccessor(0, action)

    # For illegal actions use default values
    legal = successorState.getLegalActions()
    if action not in legal:
        features['food-eaten'] = False
        features['ghost-scared-closer'] = False
        features['ghost-attack-closer'] = False
    else:

        # Food Eaten is true if difference between original and successor number of food more than 0
        features['food-eaten'] = (gameState.getNumFood() - successorState.getNumFood()) > 0

        # If Pacman ate Power pellet all ghosts are scared for certain time period, as soon as a
        # ghost is eaten it returns home and becames attacker ghost, so there may be cases where game
        # has combination of scared and attacker ghosts

        # Check if Pacman is moving closer to a scared or an attacker ghost
        ghostScaredCloser = False
        ghostAttackCloser = False

        oldPacmanPosition = gameState.getPacmanPosition()
        newPacmanPosition = successorState.getPacmanPosition()

        for index in range(1, gameState.getNumAgents()):
            oldGhost = gameState.getGhostState(index)
            newGhost = successorState.getGhostState(index)
            oldGhostPosition = oldGhost.getPosition()
            newGhostPosition = newGhost.getPosition()
            oldPacmanGhostDistance = manhattanDistance(oldGhostPosition, oldPacmanPosition)
            newPacmanGhostDistance = manhattanDistance(newGhostPosition, newPacmanPosition)
            if (newPacmanGhostDistance < oldPacmanGhostDistance):
                if (newGhost.scaredTimer > 0):
                    ghostScaredCloser = True
                else:
                    ghostAttackCloser = True

        features['ghost-scared-closer'] = ghostScaredCloser
        features['ghost-attack-closer'] = ghostAttackCloser
        

        food = successorState.getFood()
        walls = successorState.getWalls()

        # Get the closest food to the successor using Breadth First Search, but only
        # go up to a distance of 5, any value higher than this doesn't really matter
        
        closestFood = closestFoodSearch(newPacmanPosition, food, walls, maximum_distance = 5)
        if closestFood is not None:
            features['closest-food'] = closestFood 
        



        """
        ****************** CLASSIFIERS NOT USED ******************************
        """


        """
        # Get the closest ghost
        closestGhost = closestGhostSearch(newPacmanPosition, successorState.getGhostPositions(), walls)
        if closestGhost is not None:
            features['closest-ghost'] = closestGhost
        """



        # Moving closer to ghost
        """
        newGhostStates = successorState.getGhostStates()
        oldPos = gameState.getPacmanPosition()
        oldGhostStates = gameState.getGhostStates()
        oldClosestGhost = min([manhattanDistance(ghostState.getPosition(), oldPos) for ghostState in oldGhostStates])
        newPos = successorState.getPacmanPosition()
        newClosestGhost = min([manhattanDistance(ghostState.getPosition(), newPos) for ghostState in newGhostStates])
        features['ghost-closer'] = newClosestGhost < oldClosestGhost

        """



    return features


def closestFoodSearch(pacmanPosition, food, walls, maximum_distance = 100):
    """
    Use Breadth First Search to find the closest food.
    Used priority queue to store list of positions to try, the utils.PriorityQueue didn't
    allow me to get distance (priority) from the pop function, so used heapq directly.

    """
    open = []
    closed = set()
    
    heapq.heappush(open, (pacmanPosition, 0))

    closed = set()

    while open:
        position, distance = heapq.heappop(open)
        if (position) in closed:
            continue
        closed.add(position)
        # Found food, return distance from
        x, y = position 
        if food[x][y]:
            return distance

        # Add legal neighbours to Priority Queue
        neighbours = Actions.getLegalNeighbors(position, walls)
        for neighbour in neighbours:
            if (distance < maximum_distance): 
                heapq.heappush(open, (neighbour, distance + 1))

    # Didn't find food
    return None


def closestGhostSearch(pacmanPosition, ghosts, walls):
    """
    NOT USED - Algorithm runs quite slowly and benefits were minimal - KEPT FOR PROSPERITY
    Use Breadth First Search to find the closest GHOST.
    Used priority queue to store list of positions to try, the utils.PriorityQueue didn't
    allow me to get distance (priority) from the pop function, so used heapq directly.

    """
    open = []
    closed = set()
    
    heapq.heappush(open, (pacmanPosition, 0))

    closed = set()
    closestFood = 1.0

    while open:
        position, distance = heapq.heappop(open)
        if (position) in closed:
            continue
        closed.add(position)
        # Found food, return distance from
        x, y = position 
        for ghostPosition in ghosts:
            if (ghostPosition == position):
                return distance

        # Add legal neighbours to Priority Queue
        neighbours = Actions.getLegalNeighbors(position, walls)
        for neighbour in neighbours:
            heapq.heappush(open, (neighbour, distance + 1))

    # Didn't find food
    return None



def do_agent(agent):
    # load data sets and convert to feature matrices and target vectors for sklearn
    # X will denote the feature matrix and y the target vector for training; Xt,yt are for testing.
    # legal and legalt are lists of lists of legal moves in the respective states encoded by X and Xt.
    # env_vec and enc_lab are the encoders used to transform the feature dicts and list to sklearn vectors
    X, y, legal, Xt, yt, legalt, enc_vec, enc_lab = prepare_dataset(agent, extract_action_features)

    #
    # QUESTION 2
    #
    
    # Create a decision tree classifier to process features
    
    clf = DecisionTreeClassifier()
    clf.fit(X, y)

    # Print decision tree
    #extract_decisiontree_rules(clf, enc_vec.get_feature_names(), enc_lab.classes_)



    #
    # QUESTION 3
    #

    
    
    # Evaluate classifier on training set
    y_pred = clf.predict(X)
    tree_training_accuracy = accuracy_score(y,y_pred)
    print('[DecisionTree] Correctly predicted on TRAINING set: {}, errors: {}'.format(sum(y==y_pred), sum(y != y_pred)))
    print(classification_report(y,y_pred))
    print('[DecisionTree] Accuracy on TRAINING set: {:.2f}'.format(tree_training_accuracy))


    # Evaluate classifier on testing set
    y_pred = clf.predict(Xt)
    tree_testing_accuracy = accuracy_score(yt,y_pred)
    print('[DecisionTree] Correctly predicted on TEST set: {}, errors: {}'.format(sum(yt==y_pred), sum(yt != y_pred)))
    print(classification_report(yt,y_pred))
    print('[DecisionTree] Accuracy on TEST set: {:.2f}'.format(tree_testing_accuracy))


    # Use cross validation so training data is split into training and validation data 10 times and get average
    crossValidateDT(X, y)
    
    # Get the best parameters to use for the DecisionTreeClassifier
    optimised_training_accuracy, optimised_testing_accuracy = bestParametersDT(X, y, Xt, yt)

    # Create diagram of learning curve to identify bias or variance
    createLearningCurveDiagram(X, y)


    

    #
    # QUESTION 4
    #

    

    # Add Neraest Neighbour classifier 
    neigh = KNeighborsClassifier()
    neigh.fit(X,y)

    y_pred = neigh.predict(X)
    neigh_training_accuracy = accuracy_score(y,y_pred)
    print('[Neighbours] Correctly predicted on TRAINING set: {}, errors: {}'.format(sum(y==y_pred), sum(y != y_pred)))
    print(classification_report(y,y_pred))
    print('[Neighbours] Accuracy on TRAINING set: {:.2f}'.format(neigh_training_accuracy))


    # Evaluate classifier on testing set
    y_pred = neigh.predict(Xt)
    neigh_testing_accuracy = accuracy_score(yt,y_pred)
    print('[Neighbours] Correctly predicted on TEST set: {}, errors: {}'.format(sum(yt==y_pred), sum(yt != y_pred)))
    print(classification_report(yt,y_pred))
    print('[Neighbours] Accuracy on TEST set: {:.2f}'.format(neigh_testing_accuracy))


    print('\nAccuracy Summary:\n')
    print("{:30}{:>10}{:>10}".format("", "Training", "Testing"))
    print("{:30}{:>10.2f}{:>10.2f}".format("Decision Tree", tree_training_accuracy, tree_testing_accuracy))
    print("{:30}{:>10.2f}{:>10.2f}".format("Decision Tree - Optimised", optimised_training_accuracy, optimised_testing_accuracy))
    print("{:30}{:>10.2f}{:>10.2f}".format("KNearest Neighbour", neigh_training_accuracy, neigh_testing_accuracy))


    
    #
    # QUESTION 5
    #

    # Find optimal value of n_neighbours for the KNeighborsClassifier
    for i in range(1, 11):
        neigh = KNeighborsClassifier(n_neighbors=i)
        neigh.fit(X,y)

        y_pred = neigh.predict(X)
        neigh_training_accuracy = accuracy_score(y,y_pred)

        # Evaluate classifier on testing set
        y_pred = neigh.predict(Xt)
        neigh_testing_accuracy = accuracy_score(yt,y_pred)

        neighbourStr = "KNearest Neighbour({})".format(i)
        print("{:30}{:>10.2f}{:>10.2f}".format(neighbourStr, neigh_training_accuracy, neigh_testing_accuracy))

    

    # Use cross validation to check best n_neighbor value
    crossValidateNeighbour(X,y)

    


    #
    # QUESTION 6: see file classifierAgents.py
    #
    



def crossValidateDT(X, y):
    """
    Use cross validation so training data is split into training and validation data 10 times and get average
    """
    clf = DecisionTreeClassifier()
    scores = cross_val_score(clf, X, y, cv=10, scoring="accuracy")
    print("\n[DecisionTree] Cross Validation Score: ", scores, ", Mean: ", scores.mean())


def bestParametersDT(X, y, Xt, yt):
    """
    Get the best parameters to use for the DecisionTreeClassifier
    """

    parameters = {
        'max_depth': list(range(2, 30)),
        'min_samples_split': (2,),
        'min_samples_leaf': (1,),
        'splitter': ('random', 'best'),
        'max_features': list(range(1,5))
    }
    clf = DecisionTreeClassifier()
    # Create GridSearchCV splitting training and validation data 10 times
    grid_clf = GridSearchCV(clf, param_grid=parameters, cv=10, scoring="accuracy")
    grid_clf.fit(X, y)

    print('[DecisionTree - Optimised] Best score: %0.3f' % grid_clf.best_score_)
    print('[DecisionTree - Optimised] Best parameters set:')
    best_parameters = grid_clf.best_estimator_.get_params()
    for param_name in sorted(parameters.keys()):
        print('\t%s: %r' % (param_name, best_parameters[param_name]))


    y_pred = grid_clf.predict(X)
    optimised_training_accuracy = accuracy_score(y,y_pred)
    print(classification_report(y,y_pred))
    print('[DecisionTree - Optimised] Accuracy on TRAINING set: {:.2f}'.format(optimised_training_accuracy))

    y_pred = grid_clf.predict(Xt)
    print(classification_report(yt, y_pred))
    optimised_testing_accuracy = accuracy_score(yt,y_pred)
    print('[Decision Tree - Optimised] Accuracy on TEST set: {:.2f}'.format(optimised_testing_accuracy))

    return optimised_training_accuracy, optimised_testing_accuracy


def createLearningCurveDiagram(X, y):
    """
    Create diagram of learning curve to identify bias or variance
    """

    plt.figure()
    plt.title("{} Agent Learning Curve (Decision Tree)".format(agent))
    plt.xlabel("Training examples")
    plt.ylabel("Score")

    clf = DecisionTreeClassifier()
    cv = ShuffleSplit(n_splits=100, test_size=0.2, random_state=0)
    train_sizes=np.linspace(.1, 1.0, 5)
    train_sizes, train_scores, test_scores = learning_curve(clf, X, y, cv=cv, n_jobs=4, train_sizes=train_sizes)
    train_scores_mean = np.mean(train_scores, axis=1)
    train_scores_std = np.std(train_scores, axis=1)
    test_scores_mean = np.mean(test_scores, axis=1)
    test_scores_std = np.std(test_scores, axis=1)
    plt.grid()

    plt.fill_between(train_sizes, train_scores_mean - train_scores_std, train_scores_mean + train_scores_std, alpha=0.1, color="r")
    plt.fill_between(train_sizes, test_scores_mean - test_scores_std, test_scores_mean + test_scores_std, alpha=0.1, color="g")
    plt.plot(train_sizes, train_scores_mean, 'o-', color="r", label="Training score")
    plt.plot(train_sizes, test_scores_mean, 'o-', color="g", label="Cross-validation score")

    plt.legend(loc="best")

    plt.show()


def crossValidateNeighbour(X,y):
    """
    Use cross validation to check best n_neighbor value
    """

    cv_scores = []
    neighbors = list(range(1,11))

    for i in neighbors:
        neigh = KNeighborsClassifier(n_neighbors=i)
        scores = cross_val_score(neigh, X, y, cv=10, scoring="accuracy")
        cv_scores.append(scores.mean())

    # Convert to errors
    neighbour_errors = [1 - x for x in cv_scores]
    best_neighbour = neighbors[neighbour_errors.index(min(neighbour_errors))]
    print("[CROSS VALIDATION] Best n_neighbor value is {}".format(best_neighbour))


    # Show  a graph of the error rate by n_neighbor value
    plt.plot(neighbors, neighbour_errors)
    plt.xlabel("Agent: {} - n_neighbor value".format(agent))
    plt.ylabel("Miscalculation Error")
    plt.show()

if __name__ == '__main__':
 #   for agent in ['food', 'leftturn', 'random', 'stop', 'suicide']: # pick agents you need to consider
    for agent in ['food', 'random', 'stop', 'suicide']: # pick agents you need to consider
    #for agent in ['food']: # pick agents you need to consider
        print("AGENT", agent, "\n" + "=" * 40, "\n")
        do_agent(agent)
        print("\n\n\n")
